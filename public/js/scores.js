/*jshint globals: true, browser: true, jquery: true*/
$(window).load(function() {
  var nickForm = $("#setNick");
  var nickError = $("#nickError");
  var nickBox = $("#nickname");
  var users = $("#users");
 var msgForm = $('#sendMsg');
 var msg = $('#msg');
 var chat = $("#chat");
 var socket;

   if (!socket || !socket.connected) {
       socket = io({forceNew: true});
   }

   nickForm.submit (function(event) {
     event.preventDefault();
     socket.emit('new user', nickBox.val(), function (data) {
      if(data) {
        $("#nickWrap").hide();
        $("#contentWrap").show();
        $("#users").show();
      } else {
        nickError.html("Login zajęty, wybierz inny");
      }
     });
     nickBox.val('');
   });

   socket.on("usernames", function(data) {
     var html = '';
     for(var i = 0; i < data.length; i++) {
       html += data[i] + '<br />';
     }
     users.html("<p style='font-style: oblique;'>Użytkownicy na czacie:</p> "+html);
   });

   msgForm.submit(function (event) {
     event.preventDefault();
     socket.emit("sendMessage", msg.val());
     msg.val("");
   });

   socket.on("newMessage", function (received) {
     chat.append ("<b>" + received.nick + ":</b> " + received.msg + "<br />");
   });
  //  socket.on('pages', function(data){
  //    console.log(data.your);
  //  });
  //  socket.on('sendScore', function (lol) {
  //    $('.tab').remove();
  //    var content = "";
  //    $(".score").append("<table class='tab'><tr><th>Numer startowy</th><th>Kategoria 1</th><th>Kategoria 2</th><th>Kategoria 3</th><th>Kategoria 4</th><th>Kategoria 5</th></tr></table>");
  //    for(var i = 0; i < lol.length; i++) {
  //      content +=    "<tr><td>"+lol[i].startingNumber+"</td>"+
  //                    "<td>"+lol[i].categoryOne+"</td>"+
  //                    "<td>"+lol[i].categoryTwo+"</td>"+
  //                    "<td>"+lol[i].categoryThree+"</td>"+
  //                    "<td>"+lol[i].categoryFour+"</td>"+
  //                    "<td>"+lol[i].categoryFive+"</td></tr>";
  //    }
  //    $(".tab").append(content);
  // });
  socket.on('sendScore', function (scores) {
    $('.tab').remove();
    var content = "";
    $(".score").append("<table class='tab'><tr><th>Nr startowy konia</th><th>Średnia wszystkich ocen</th></tr></table>");
    for(var i = 0; i < scores.length; i++) {
      content +=    "<tr><td>"+scores[i].startNumb+
                    "</td><td>"+scores[i].average+
                    "</td></tr>";
    }
    $(".tab").append(content);
 });

 socket.on("eachHorse", function(result) {
    alert("Nowy wynik! \n Numer startowy konia: " + result.startingNumber + "\n Ocena kategorii pierwszej: " + result.category1 +
    "\n Ocena kategorii drugiej: " + result.category2 + "\n Ocena kategorii trzeciej: " + result.category3 +
    "\n Ocena kategorii czwartej: " + result.category4 + "\n Ocena kategorii piątej: " + result.category5 +
    "\n Oceniał sędzia z identyfikatorem: " + result.judgeId);
 });

 socket.on("eachHorse2", function(result2) {
    alert("Nowy wynik! \n Numer startowy konia: " + result2.startingNumber + "\n Ocena kategorii pierwszej: " + result2.category1 +
    "\n Ocena kategorii drugiej: " + result2.category2 + "\n Ocena kategorii trzeciej: " + result2.category3 +
    "\n Ocena kategorii czwartej: " + result2.category4 + "\n Ocena kategorii piątej: " + result2.category5 +
    "\n Oceniał sędzia z identyfikatorem: " + result2.judgeId);
 });

 socket.on("eachHorse3", function(result3) {
    alert("Nowy wynik! \n Numer startowy konia: " + result3.startingNumber + "\n Ocena kategorii pierwszej: " + result3.category1 +
    "\n Ocena kategorii drugiej: " + result3.category2 + "\n Ocena kategorii trzeciej: " + result3.category3 +
    "\n Ocena kategorii czwartej: " + result3.category4 + "\n Ocena kategorii piątej: " + result3.category5 +
    "\n Oceniał sędzia z identyfikatorem: " + result3.judgeId);
 });

 socket.on("eachHorse4", function(result4) {
    alert("Nowy wynik! \n Numer startowy konia: " + result4.startingNumber + "\n Ocena kategorii pierwszej: " + result4.category1 +
    "\n Ocena kategorii drugiej: " + result4.category2 + "\n Ocena kategorii trzeciej: " + result4.category3 +
    "\n Ocena kategorii czwartej: " + result4.category4 + "\n Ocena kategorii piątej: " + result4.category5 +
    "\n Oceniał sędzia z identyfikatorem: " + result4.judgeId);
 });

 socket.on("eachHorse5", function(result5) {
    alert("Nowy wynik! \n Numer startowy konia: " + result5.startingNumber + "\n Ocena kategorii pierwszej: " + result5.category1 +
    "\n Ocena kategorii drugiej: " + result5.category2 + "\n Ocena kategorii trzeciej: " + result5.category3 +
    "\n Ocena kategorii czwartej: " + result5.category4 + "\n Ocena kategorii piątej: " + result5.category5 +
    "\n Oceniał sędzia z identyfikatorem: " + result5.judgeId);
 });

});
