/*jshint globals: true, browser: true, jquery: true*/
$(window).load(function () {
    // executes when complete page is fully loaded, including all frames, objects and images
    var formField = $("#formField");
    var formFieldTwo = $("#formFieldTwo");
    var formFieldThree = $("#formFieldThree");
    var formFieldFour = $("#formFieldFour");
    var formFieldFive = $("#formFieldFive");
    var startNum = $("#startNum").val();
    var firstCat = $("#firstCat").val();
    var secondCat = $("#secondCat").val();
    var thirdCat = $("#thirdCat").val();
    var forthCat = $("#forthCat").val();
    var fifthCat = $("#fifthCat").val();
    //var login = $("#login").val();
    var token = $("#token");
    var veryfy = $("#veryfy");
    var verification = $("#verification");
    var socket;
    var assign;

    if (!socket || !socket.connected) {
        socket = io({
            forceNew: true
        });
    }
    socket.on('connect', function () {
        var id = socket.io.engine.id;
        //wysylam sesje socket.id danego sedziego
        socket.emit('fiveJudges', id);
        //przyjmuje wylosowana liczbe i przypisuje ja do zmiennej assign
        socket.on('lottery', function (data) {
            //if('/#'+id === data)
            $("#open").show();
            assign = data;
        });
    });
    $("#open").click(function (event) {
        $("#open").hide();

        console.log("Wylosowano numer: " + assign);
        // $("#auth").click(function () {
        //   socket.emit("check", login);
        // });
        //wysyla wylosowana liczbe do serwera
        socket.emit('pickJudge', assign);
        verification.show();
        socket.on('key', function (pass) {
            console.log(pass);
            veryfy.click(function () {
                if (token.val() == pass) {
                    verification.hide();
                    formField.show();
                }
            });
        });


        function validateForm() {

            if ($("#startNum").val() !== 1 || $("#startNum2").val() !== 2 || $("#startNum3").val() !== 3 || $("#startNum4").val() !== 4 || $("#startNum5").val() !== 5) {
                //$( ".error" ).append( "<p>Test</p>" );
            }
        }


        $("#przeslij").click(function (event) {
            //validateForm();
            var newMark = {
                startingNumber: 1,
                category1: $("#firstCat").val(),
                category2: $("#secondCat").val(),
                category3: $("#thirdCat").val(),
                category4: $("#forthCat").val(),
                category5: $("#fifthCat").val()
            };
            var vote = {
                startingNumber: 1,
                category1: $("#firstCat").val(),
                category2: $("#secondCat").val(),
                category3: $("#thirdCat").val(),
                category4: $("#forthCat").val(),
                category5: $("#fifthCat").val(),
                judgeId: assign
            };
            socket.emit("lastScore", vote);
            $.ajax({
                url: "/horseRate",
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify(newMark),
                processData: false,
                contentType: 'application/json; charset=UTF-8',
                success: function (data) {
                    alert("OK" + data);
                },
                error: function (xhr, desc, err) {


                }
            });
            //event.preventDefault();
            //formField.hide();
            formField.hide();
            formFieldTwo.show();
            return false;

        });

        $("#przeslij2").click(function (event) {
            var newMarkTwo = {
                startingNumber: 2,
                category1: $("#firstCat2").val(),
                category2: $("#secondCat2").val(),
                category3: $("#thirdCat2").val(),
                category4: $("#forthCat2").val(),
                category5: $("#fifthCat2").val()
            };
            var vote2 = {
                startingNumber: 2,
                category1: $("#firstCat2").val(),
                category2: $("#secondCat2").val(),
                category3: $("#thirdCat2").val(),
                category4: $("#forthCat2").val(),
                category5: $("#fifthCat2").val(),
                judgeId: assign
            };
            socket.emit("lastScore2", vote2);
            $.ajax({
                url: "/horseRate",
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify(newMarkTwo),
                processData: false,
                contentType: 'application/json; charset=UTF-8',
                success: function (data) {}
            });

            formFieldTwo.hide();
            formFieldThree.show();
            return false;
        });


        $("#przeslij3").click(function (event) {
            var newMarkThree = {
                startingNumber: 3,
                category1: $("#firstCat3").val(),
                category2: $("#secondCat3").val(),
                category3: $("#thirdCat3").val(),
                category4: $("#forthCat3").val(),
                category5: $("#fifthCat3").val()
            };

            var vote3 = {
                startingNumber: 3,
                category1: $("#firstCat3").val(),
                category2: $("#secondCat3").val(),
                category3: $("#thirdCat3").val(),
                category4: $("#forthCat3").val(),
                category5: $("#fifthCat3").val(),
                judgeId: assign
            };
            socket.emit("lastScore3", vote3);

            $.ajax({
                url: "/horseRate",
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify(newMarkThree),
                processData: false,
                contentType: 'application/json; charset=UTF-8',
                success: function (data) {}
            });

            formFieldThree.hide();
            formFieldFour.show();
            return false;
        });

        $("#przeslij4").click(function (event) {
            var newMarkFour = {
                startingNumber: 4,
                category1: $("#firstCat4").val(),
                category2: $("#secondCat4").val(),
                category3: $("#thirdCat4").val(),
                category4: $("#forthCat4").val(),
                category5: $("#fifthCat4").val()
            };

            var vote4 = {
                startingNumber: 4,
                category1: $("#firstCat4").val(),
                category2: $("#secondCat4").val(),
                category3: $("#thirdCat4").val(),
                category4: $("#forthCat4").val(),
                category5: $("#fifthCat4").val(),
                judgeId: assign
            };
            socket.emit("lastScore4", vote4);

            $.ajax({
                url: "/horseRate",
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify(newMarkFour),
                processData: false,
                contentType: 'application/json; charset=UTF-8',
                success: function (data) {}
            });

            formFieldFour.hide();
            formFieldFive.show();
            return false;
        });

        $("#przeslij5").click(function (event) {
            var newMarkFive = {
                startingNumber: 5,
                category1: $("#firstCat5").val(),
                category2: $("#secondCat5").val(),
                category3: $("#thirdCat5").val(),
                category4: $("#forthCat5").val(),
                category5: $("#fifthCat5").val()
            };

            var vote5 = {
                startingNumber: 5,
                category1: $("#firstCat5").val(),
                category2: $("#secondCat5").val(),
                category3: $("#thirdCat5").val(),
                category4: $("#forthCat5").val(),
                category5: $("#fifthCat5").val(),
                judgeId: assign
            };
            socket.emit("lastScore5", vote5);

            $.ajax({
                url: "/horseRate",
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify(newMarkFive),
                processData: false,
                contentType: 'application/json; charset=UTF-8',
                success: function (data) {}
            });
            formFieldFive.hide();
            $("#thanks").show();
            return false;
        });
    });



    //  socket.emit('wyniki', {
    //    st: startNum,
    //    first: firstCat,
    //    second: secondCat,
    //    third: thirdCat,
    //    forth: forthCat,
    //    fifth: fifthCat
    //  });


});