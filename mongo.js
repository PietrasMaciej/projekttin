/*jshint globals: true, node: true*/

var mongoose = require("mongoose");
var async = require("async");

mongoose.connect('mongodb://localhost/Vote', function (error) {
  if (error) {
    console.log("Nie połączono z bazą danych: " + error);
  } else {
    console.log("Połączono z bazą danych.");
  }
});

var horse = mongoose.model('horses', new mongoose.Schema({
    _id: String,
    startingNumber: Number,
    categoryOne: Number,
    categoryTwo: Number,
    categoryThree: Number,
    categoryFour: Number,
    categoryFive: Number
}));

var judge = mongoose.model('judges', new mongoose.Schema({
    _id: String,
    number: Number,
    username: String,
    token: Number
    //horseId:        { mark: String, ref: 'horses' }
}));

function randomInt(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

var judge1 = new judge({ _id: mongoose.Types.ObjectId(), number: 1, username: 'qqq', token: randomInt(1000,9999) });
var judge2 = new judge({ _id: mongoose.Types.ObjectId(), number: 2, username: 'www', token: randomInt(1000,9999) });
var judge3 = new judge({ _id: mongoose.Types.ObjectId(), number: 3, username: 'eee', token: randomInt(1000,9999) });
var judge4 = new judge({ _id: mongoose.Types.ObjectId(), number: 4, username: 'rrr', token: randomInt(1000,9999) });
var judge5 = new judge({ _id: mongoose.Types.ObjectId(), number: 5, username: 'ttt', token: randomInt(1000,9999) });
var judge6 = new judge({ _id: mongoose.Types.ObjectId(), number: 6, username: 'yyy', token: randomInt(1000,9999) });
var judge7 = new judge({ _id: mongoose.Types.ObjectId(), number: 7, username: 'uuu', token: randomInt(1000,9999) });
var judge8 = new judge({ _id: mongoose.Types.ObjectId(), number: 8, username: 'iii', token: randomInt(1000,9999) });
var judge9 = new judge({ _id: mongoose.Types.ObjectId(), number: 9, username: 'ooo', token: randomInt(1000,9999) });
var judge10 = new judge({ _id: mongoose.Types.ObjectId(), number: 10, username: 'ppp', token: randomInt(1000,9999) });
var judge11 = new judge({ _id: mongoose.Types.ObjectId(), number: 11, username: 'aaa', token: randomInt(1000,9999) });
var judge12 = new judge({ _id: mongoose.Types.ObjectId(), number: 12, username: 'sss', token: randomInt(1000,9999) });
var judge13 = new judge({ _id: mongoose.Types.ObjectId(), number: 13, username: 'ddd', token: randomInt(1000,9999) });
var judge14 = new judge({ _id: mongoose.Types.ObjectId(), number: 14, username: 'fff', token: randomInt(1000,9999) });
var judge15 = new judge({ _id: mongoose.Types.ObjectId(), number: 15, username: 'ggg', token: randomInt(1000,9999) });
var judge16 = new judge({ _id: mongoose.Types.ObjectId(), number: 16, username: 'hhh', token: randomInt(1000,9999) });
var judge17 = new judge({ _id: mongoose.Types.ObjectId(), number: 17, username: 'jjj', token: randomInt(1000,9999) });
var judge18 = new judge({ _id: mongoose.Types.ObjectId(), number: 18, username: 'kkk', token: randomInt(1000,9999) });
var judge19 = new judge({ _id: mongoose.Types.ObjectId(), number: 19, username: 'lll', token: randomInt(1000,9999) });
var judge20 = new judge({ _id: mongoose.Types.ObjectId(), number: 20, username: 'zzz', token: randomInt(1000,9999) });

var addJudge = function (judgeParam) {
  return function (callback) {
    judge.findOne ({
      name: judgeParam.name
    }, function(error, result) {
                 if(error) {
                   console.log(error);
                 } else {
                      if(result === null) {
                        judgeParam.save(function (err) {
                            if (err) {
                              console.log(err);
                            }
                        callback(err);
                        });
                      } else {
                          console.log("Dodawany sędzia już istnieje.");
                        }
                      }

      });
  };
};

var insert = function (callback) {
    async.parallel([
      addJudge(judge1),
      addJudge(judge2),
      addJudge(judge3),
      addJudge(judge4),
      addJudge(judge5),
      addJudge(judge6),
      addJudge(judge7),
      addJudge(judge8),
      addJudge(judge9),
      addJudge(judge10),
      addJudge(judge11),
      addJudge(judge12),
      addJudge(judge13),
      addJudge(judge14),
      addJudge(judge15),
      addJudge(judge16),
      addJudge(judge17),
      addJudge(judge18),
      addJudge(judge19),
      addJudge(judge20)], function (err) {
        if (err) {
            console.log(err);
        }
        callback();
        process.exit(0);
    });
};

async.series([insert]);
