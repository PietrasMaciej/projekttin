/*jshint globals: true, node: true*/

require('./mongo.js');

var express = require('express');
var _ = require('underscore');
var mongoose = require('mongoose');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var path = require('path');
var async = require('async');
var lessMiddleware = require("less-middleware");

var app = express();

var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);

var port = process.env.PORT || 3000;

app.use(morgan('dev'));
app.use(lessMiddleware(__dirname + "/public", {
  debug:true,
  compress: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var horse = mongoose.model('horses');
var judge = mongoose.model('judges');

app.use('/jquery', express.static(__dirname + '/bower_components/jquery/dist/'));
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname));


app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/public/watchers.html'));
});

app.get('/horseRate', function (req, res) {
  res.sendFile(path.join(__dirname, './public/judgeForm.html'));
});

app.post('/horseRate', function(req, res) {
    new horse ({
             _id: mongoose.Types.ObjectId(),
             startingNumber: req.body.startingNumber,
             categoryOne: req.body.category1,
             categoryTwo: req.body.category2,
             categoryThree: req.body.category3,
             categoryFour: req.body.category4,
             categoryFive: req.body.category5,
           }).save(function(error) {
              if(error) {
                console.log(error);
              }
              else {
                console.log("Dodano oceny konia: " + JSON.stringify(req.body));
                res.send("Dodano do bazy danych");
              }
           });
});

var clients = [];
var chosen = [];
var draw = [];
var nicknames = [];

//losowanie bez powtorzen
var noRepeat = function () {
  var result = [];
  for (var a = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], i = a.length; i--; ) {
         var random = a.splice(Math.floor(Math.random() * (i + 1)), 1)[0];
         result.push(random);

  }
  return result;
}();

// console.log("pierwszy element: " + noRepeat[0]);
// noRepeat.splice(noRepeat.indexOf(noRepeat[0]), 1);
// console.log("pierwszy element teraz: " + noRepeat[0]);
// console.log("tablica teraz: " + noRepeat);



io.sockets.on("connection", function (socket) {
  //czat
  socket.on('new user', function (data, callback) {
    if(nicknames.indexOf(data) != -1) {
      callback(false);
    } else {
      callback(true);
      socket.nickname = data;
      nicknames.push(socket.nickname);
      io.sockets.emit('usernames', nicknames);
    }
  });
  socket.on("sendMessage", function (received) {
    io.sockets.emit("newMessage", {msg: received, nick: socket.nickname});
  });


  //zawody
  clients.push(socket.id);
//console.log("Przyszedl" + clients);


//patrze czy wsrod wszystkich polaczonych socket.id sedziego jest w puli, jezeli tak to dodaje go do tablicy draw
//Jezeli tablica draw ma 5 socket.id to wysylam pod ten adres wylosowane liczby z funkcji noRepeat
socket.on("fiveJudges", function (recId) {
  for(var i = 0; i < clients.length; i++) {
    if(clients[i] === '/#'+recId)
    draw.push(clients[i]);
  }
  if(draw.length === 5) {
    io.sockets.connected[draw[0]].emit("lottery", noRepeat[0]);
    io.sockets.connected[draw[1]].emit("lottery", noRepeat[1]);
    io.sockets.connected[draw[2]].emit("lottery", noRepeat[2]);
    io.sockets.connected[draw[3]].emit("lottery", noRepeat[3]);
    io.sockets.connected[draw[4]].emit("lottery", noRepeat[4]);
    //console.log("wybrani " + chosen);
  }
});
  // if(clients.length === 5) {
  //   chosen = _.sample(clients, 5);
  //   io.sockets.connected[chosen[0]].emit("lottery", chosen[0]);
  //   io.sockets.connected[chosen[1]].emit("lottery", chosen[1]);
  //   io.sockets.connected[chosen[2]].emit("lottery", chosen[2]);
  //   io.sockets.connected[chosen[3]].emit("lottery", chosen[3]);
  //   io.sockets.connected[chosen[4]].emit("lottery", chosen[4]);
  //   //console.log("wybrani " + chosen);
  // }
// if (chosen.length ===5){
//     console.log("nastepnie wybrani tutaj " + chosen);
    //  io.sockets.connected[chosen[0]].emit("lottery", git);
    //  io.sockets.connected[chosen[1]].emit("lottery", git);
    //  io.sockets.connected[chosen[2]].emit("lottery", git);
    //  io.sockets.connected[chosen[3]].emit("lottery", git);
    //  io.sockets.connected[chosen[4]].emit("lottery", git);
// }

//jezeli odebrana od klienta wylosowana liczba zgadza sie z ktoras  bazy danych, to odsylam token
  socket.on('pickJudge', function(assign) {

    judge.findOne ({
      number: assign
    }, function(error, result) {
                 if(error) {
                   console.log(error);
                 }
                socket.emit('key', result.token);
                //console.log("Guzik" + clients);
//console.log(assign + result + result.token);
      });

  });

socket.on("lastScore", function (data) {
  io.sockets.emit("eachHorse", data);
});
socket.on("lastScore2", function (data) {
  io.sockets.emit("eachHorse2", data);
});
socket.on("lastScore3", function (data) {
  io.sockets.emit("eachHorse3", data);
});
socket.on("lastScore4", function (data) {
  io.sockets.emit("eachHorse4", data);
});
socket.on("lastScore5", function (data) {
  io.sockets.emit("eachHorse5", data);
});
//   socket.on('wyniki', function (data) {
//   // do something with data
//   console.log(data.sn);
//    socket.volatile.emit('pages', {your: 'data'});
//   // var loginDetails={
//   //   userid : userid,
//   //   username : username
//   //   };
//   //    var id = data.id
//   //    var name = data.username
//   //   socket.broadcast.emit('eventToClient',{ id: userid, name: username });
//
// });
    // horse.find ({}, function(error, result) {
    //              if(error) {
    //                console.log(error);
    //              }
    //             socket.emit('sendScore', result);
    //   });

    //wysylam wyniki z bazy danych do widzow, sortujac je

    Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

setInterval(function() {
      horse.find ({}, function(error, result) {
                   if(error) {
                     console.log(error);
                   }
                   var a = [];
                   One = {};
                   var snOne = [];
                   Two = {};
                   var snTwo = [];
                   Three = {};
                   var snThree = [];
                   var Four = {};
                   var snFour = [];
                   var Five = {};
                   var snFive = [];
                   var final = [];

                  for (var s = 0; s < result.length; s ++) {
                    if (result[s].startingNumber === 1) {
                      snOne[0] = 1; // numer startowy konia
                      snOne[s+1]=(result[s].categoryOne + result[s].categoryTwo + result[s].categoryThree + result[s].categoryFour + result[s].categoryFive);
                    }
                    else if (result[s].startingNumber === 2) {
                      snTwo[0] = 2;
                      snTwo[s+1]=(result[s].categoryOne + result[s].categoryTwo + result[s].categoryThree + result[s].categoryFour + result[s].categoryFive);
                    }
                    else if (result[s].startingNumber === 3) {
                      snThree[0] = 3;
                      snThree[s+1]=(result[s].categoryOne + result[s].categoryTwo + result[s].categoryThree + result[s].categoryFour + result[s].categoryFive);
                    }
                    else if (result[s].startingNumber === 4) {
                      snFour[0] = 4;
                      snFour[s+1]=(result[s].categoryOne + result[s].categoryTwo + result[s].categoryThree + result[s].categoryFour + result[s].categoryFive);
                    }
                    else if (result[s].startingNumber === 5) {
                      snFive[0] = 5;
                      snFive[s+1]=(result[s].categoryOne + result[s].categoryTwo + result[s].categoryThree + result[s].categoryFour + result[s].categoryFive);
                    }
                  }
                  snOne.clean(undefined);
                  snTwo.clean(undefined);
                  snThree.clean(undefined);
                  snFour.clean(undefined);
                  snFive.clean(undefined);

                  var countOne = (_.reduce(_.rest(snOne), function(memo, num){ return memo + num; }, 0))/((_.rest(snOne).length)*5);
                  var countTwo = (_.reduce(_.rest(snTwo), function(memo, num){ return memo + num; }, 0))/((_.rest(snTwo).length)*5);
                  var countThree = (_.reduce(_.rest(snThree), function(memo, num){ return memo + num; }, 0))/((_.rest(snThree).length)*5);
                  var countFour = (_.reduce(_.rest(snFour), function(memo, num){ return memo + num; }, 0))/((_.rest(snFour).length)*5);
                  var countFive = (_.reduce(_.rest(snFive), function(memo, num){ return memo + num; }, 0))/((_.rest(snFive).length)*5);



                  var finalScoreOne = {
                    startNumb: snOne[0],
                    average: countOne
                  };

                  var finalScoreTwo = {
                    startNumb: snTwo[0],
                    average: countTwo
                  };

                  var finalScoreThree = {
                    startNumb: snThree[0],
                    average: countThree
                  };

                  var finalScoreFour = {
                    startNumb: snFour[0],
                    average: countFour
                  };

                  var finalScoreFive = {
                    startNumb: snFive[0],
                    average: countFive
                  };


                  if (finalScoreOne.startNumb === undefined || isNaN(finalScoreOne.average)) {
                    console.log("Nie dodano");
                    }
                  else {
                    final.push(finalScoreOne);
                  }

                  if (finalScoreTwo.startNumb === undefined || isNaN(finalScoreTwo.average)) {
                  console.log("Nie dodano");
                  }
                  else {
                    final.push(finalScoreTwo);
                  }

                  if (finalScoreThree.startNumb === undefined || isNaN(finalScoreThree.average)) {
                    console.log("Nie dodano");
                    }
                  else {
                    final.push(finalScoreThree);
                  }

                  if (finalScoreFour.startNumb === undefined || isNaN(finalScoreFour.average)) {
                    console.log("Nie dodano");
                    }
                  else {
                    final.push(finalScoreFour);
                  }

                  if (finalScoreFive.startNumb === undefined && isNaN(finalScoreFive.average)) {
                      console.log("Nie dodano");
                      }
                  else {
                    final.push(finalScoreFive);
                  }

                   var sortedArray = _.sortBy(final, 'average');
//                    var sortedArray = _.sortBy(result, function(result) {
//   return result.categoryOne + result.categoryTwo + result.categoryThree + result.categoryFour + result.categoryFive;
// });
                  socket.emit('sendScore', sortedArray.reverse());
        });
 }, 1000);


//gdy ktos sie rozlaczy to wykonuja sie operacje tutaj
      socket.on('disconnect', function() {
              if(!socket.nickname) return; //czat
              nicknames.splice(nicknames.indexOf(socket.nickname), 1); //czat
              io.sockets.emit('usernames', nicknames); //czat
        //console.log("Prawie uciekl" + clients);
              clients.splice( clients.indexOf(socket.id), 1 ); //klienci z tablicy clients sa usuwani
              //console.log("Ucieka" + clients);
          });

});

httpServer.listen(port, function () {
    console.log('Serwer działa na porcie ' + port);
});
